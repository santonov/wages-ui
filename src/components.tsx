import { ApolloClient, ApolloProvider, InMemoryCache, useLazyQuery, useMutation, useQuery } from '@apollo/client';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {
  BrowserRouter as Router, Link, Route, Switch
} from "react-router-dom";
import * as Q from './graphql/queries.graphql';

const client = new ApolloClient({
  uri: 'http://localhost:8080/graphql',
  cache: new InMemoryCache()
});

export function renderAll() {
  ReactDOM.render(
    <ApolloProvider client={client}>
      <Page />
    </ApolloProvider>,
    document.getElementById('root')
  )
}

interface ProcessProps {
  id: string,
  status: Q.Status
}

function ProcessRow(props: ProcessProps) {
  return <tr key={props.id}>
    <td>{props.id}</td>
    <td>{props.status?.description}</td>
    <td>
      <ManageProcessButtons id={props.id} processStatus={props.status} />
    </td>
    <td>
      {props.status?.percentsCompleted}
    </td>
  </tr>;
}


interface ManageProcessButtonProps {
  id: string,
  processStatus: Q.Status
}

function ManageProcessButtons(props: ManageProcessButtonProps) {
  const [manageProcess] = useMutation<Q.ManageProcessMutation, Q.ManageProcessMutationVariables>(Q.ManageProcessDocument);
  const [processState, setProcessState] = React.useState(props.processStatus);

  enum ProcessStatus {
    Inactive,
    Created,
    Active
  }

  const processStatusButtons: Record<ProcessStatus, Q.ProcessAction[]> = {
    [ProcessStatus.Inactive]: [Q.ProcessAction.Create],
    [ProcessStatus.Created]: [Q.ProcessAction.Start, Q.ProcessAction.Cancel],
    [ProcessStatus.Active]: []
  };

  const triggerProcessAction = (processId: string, action: Q.ProcessAction) => {
    const data = manageProcess({
      variables: {
        action: action,
        id: processId
      }
    });
  }

  const makeButton = (processId: string, action: Q.ProcessAction) => {
    return <ProcessButton key={action} label={action} onClick={() => triggerProcessAction(processId, action)} />
  };

  const currentStatus =
    props.processStatus == null ? ProcessStatus.Inactive
      : props.processStatus.percentsCompleted == 0 ? ProcessStatus.Created
        : ProcessStatus.Active;

  return <div>
    {processStatusButtons[currentStatus].map(e => makeButton(props.id, e))}
    <RefreshProcessStateButton key="refresh" id={props.id} onNewState={setProcessState} />
  </div>;
}

interface ProcessButtonProps {
  onClick: Function,
  label: string
}
function ProcessButton(props: ProcessButtonProps) {
  return <button className="btn btn-outline-secondary" onClick={() => props.onClick()}>{props.label}</button>;
}
interface RefreshProcessStateButtonProps {
  id: string,
  onNewState: (newState: Q.Status) => void
}

function RefreshProcessStateButton(props: RefreshProcessStateButtonProps) {
  const [query, result] = useLazyQuery<Q.ProcessesQuery, Q.ProcessesQueryVariables>(Q.ProcessesDocument, {
    onCompleted: () => {
      props.onNewState(result.data.processes[0].status);
    }
  });
  const queryProcess = (id: string) => {
    // XXX: I bet, there is a better way to do that
    if (result.refetch != null) {
      result.refetch({ id: id });
    } else {
      query({ variables: { id: id } });
    }
  }
  return <ProcessButton label="&#x21bb;" onClick={() => queryProcess(props.id)} />
}


function Page() {
  return (
    <Router>
      <div> <nav className="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
        <Link to="/" className="navbar-brand" href="#">Yay, Wages!</Link>
      </nav>
        <main role="main" className="container" style={{ paddingTop: 70 }}>
          <Switch>
            <Route path="/">
              <ProcessTable />
            </Route>
            <Route path="/process/:id" component={ProcessRow}>
              Nothing here =(
            </Route>
          </Switch>
        </main>
      </div>
    </Router>);
}

function ProcessTable() {
  const { loading, error, data } = useQuery<Q.ProcessesQuery, Q.ProcessesQueryVariables>(Q.ProcessesDocument);
  if (loading) return <div>loading...</div>
  if (error) return <div> Error: {error} </div>

  console.log("rendering processes");
  console.log(data);
  return <table className="table">
    <thead>
      <tr>
        <th> Id </th>
        <th> Current Stage </th>
        <th colSpan={2} > Manage </th>
      </tr>
    </thead>
    <tbody>
      {data.processes.map(e => ProcessRow({ id: e.id, status: e.status }))}
    </tbody>
  </table>;
}

